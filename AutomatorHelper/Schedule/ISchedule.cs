﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomatorHelper.Schedule
{
    /// <summary>
    /// Расписание
    /// </summary>
    public interface ISchedule
    {
        /// <summary>
        /// Уникальный Id расписания
        /// </summary>
        long Id { get; }

        /// <summary>
        /// Самоопределение времени совпадения
        /// </summary>
        /// <returns></returns>
        bool IsOccurentIn(DateTimeOffset date);

        /// <summary>
        /// Аргумент
        /// </summary>
        object Arg { get; }

        /// <summary>
        /// Приоритет
        /// </summary>
        int Priority { get; }

        /// <summary>
        /// Тип обработчика (DI dependent)
        /// </summary>
        Type WorkerType { get; }

        /// <summary>
        /// Сообщить менеджеру расписаний, что не нужно прерывать задачи если занят или приоритет ниже
        /// </summary>
        bool WorkInParallel { get; }

        /// <summary>
        /// Id расписаний, с которыми конфликтует данное расписание
        /// </summary>
        HashSet<long> Conflicts { get; set; }
    }
}
