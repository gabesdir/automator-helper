﻿using AutomatorHelper.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Schedule
{
    /// <summary>
    /// 
    /// </summary>
    public class SchedulePicker
    {
        private readonly List<IScheduleProvider> scheduleProviders = new List<IScheduleProvider>();

        /// <summary>
        /// Помогает искать подходящие расписания на сейчас
        /// </summary>

        /// <summary>
        /// Добавить провайдеров таблицы расписаний
        /// </summary>
        /// <param name="providers"></param>
        public SchedulePicker AddScheduleProviders(IScheduleProvider[] providers)
        {
            foreach (var provider in providers)
            {
                scheduleProviders.Add(provider);
            }
            return this;
        }

        /// <summary>
        /// Добавить одного провайдера
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        public SchedulePicker AddScheduleProvider(IScheduleProvider provider)
        {
            this.scheduleProviders.Add(provider);
            return this;
        }

        /// <summary>
        /// Возвращает совпадения из добавленых провайдеров расписаний
        /// </summary>
        /// <returns></returns>
        public async Task< List<ISchedule>> GetNowOccurences()
        {
            //trim to minutes
            var trimmed = new DateTimeOffset(DateTime.Now.TrimToMinutes());

            var wholeTable = new TimeTable();
            foreach (var p in scheduleProviders)
            {
                wholeTable.UnionWith(await p.GetSchedules());
            }

            return wholeTable.Where(s => s.IsOccurentIn(trimmed)).ToList();
        }
    }

    
}
