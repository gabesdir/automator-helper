﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomatorHelper.Schedule
{
    /// <summary>
    /// Работник
    /// </summary>
    public interface IWorker
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <param name="cancellationToken"></param>
        void Work(object arg, CancellationToken cancellationToken);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task WorkAsync(object arg, CancellationToken cancellationToken);
    }
}
