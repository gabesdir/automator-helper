﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AutomatorHelper.Schedule
{
    /// <summary>
    /// Сосстояние worker task
    /// </summary>
    public enum WorkState : int
    {
        /// <summary>
        /// Не занят
        /// </summary>
        Idle = 0,
        /// <summary>
        /// В работе
        /// </summary>
        Work = 1,
        /// <summary>
        /// Отменяется
        /// </summary>
        Cancellation = 2
    }

    /// <summary>
    /// Рабочий набор
    /// </summary>
    public class Workset
    {
        /// <summary>
        /// Id задания (уникальный)
        /// </summary>
        public long WorkId { get; private set; }

        /// <summary>
        /// Набор данных для обработчика задания
        /// </summary>
        public object Argument { get; private set; }

        /// <summary>
        /// Флаг: невытесняющая работа (на уровне приложения, но НЕ системы)
        /// </summary>
        public bool CanParallel { get; private set; }

        /// <summary>
        /// Относительный приоритет задания
        /// </summary>
        public int Priority { get; private set; }

        /// <summary>
        /// Объект задания, для запуска оработчика
        /// </summary>
        public Task Task { get; private set; }

        /// <summary>
        /// Источник токена отмены для запланированного прерывания работы
        /// </summary>
        public CancellationTokenSource CancellationTokenSource { get; } = new CancellationTokenSource();

        /// <summary>
        /// Состояние работы
        /// </summary>
        public WorkState WorkState { get; set; } = WorkState.Idle;

        /// <summary>
        /// Тип обработчика задания для разрешения зависимостей через DI контейнер
        /// </summary>
        public Type WorkerType { get; private set; }

        /// <summary>
        /// Добавить ссылку на Task
        /// </summary>
        /// <param name="work"></param>
        public void AssignTask(Task work)
        {
            work = work ?? throw new ArgumentException("Invalid work Task");
            if (this.Task != null)
            {
                throw new Exception("Cannot assign task: already assigned");
            }
            this.Task = work;
        }

        /// <summary>
        /// Constructor from ISchedule
        /// </summary>
        /// <param name="schedule"></param>
        public Workset(ISchedule schedule)
        {
            this.WorkId = schedule.Id;
            this.Argument = schedule.Arg;
            this.CanParallel = schedule.WorkInParallel;
            this.Priority = schedule.Priority;

            this.AssignWorkerType(schedule.WorkerType);
        }

        /// <summary>
        /// Constructor with params
        /// </summary>
        /// <param name="id"></param>
        /// <param name="arg"></param>
        /// <param name="parallel"></param>
        /// <param name="priority"></param>
        /// <param name="workerType"></param>
        public Workset(long id, object arg, bool parallel, int priority, Type workerType)
        {
            this.WorkId = id;
            this.Argument = arg;
            this.CanParallel = parallel;
            this.Priority = priority;

            this.AssignWorkerType(workerType);
        }

        private void AssignWorkerType(Type workerType)
        {
            if (this.WorkerType != null)
            {
                throw new Exception("Cannot assign worker: already assigned");
            }

            if (workerType.GetInterfaces().Contains(typeof(IWorker)))
            {
                this.WorkerType = workerType;
            }
            else
            {
                throw new Exception($"Invalid worker type {(workerType.Name)}: does not implements IWorker");
            }
        }
    }
}
