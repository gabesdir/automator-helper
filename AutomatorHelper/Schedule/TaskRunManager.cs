﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AutomatorHelper.Schedule;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AutomatorHelper.Schedule
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskRunManager
    {
        private const int MaxSafeRunnigTasks = 200;

        private readonly IServiceProvider serviceProvider;

        /// <summary>
        /// Рабочий набор
        /// </summary>
        public BlockingCollection<Workset> WorkSets { get; } = new BlockingCollection<Workset>();

        private readonly ILogger logger;

        /// <summary>
        /// Все слоты полностью заняты
        /// </summary>
        public bool IsBusy => WorkSets.Count >= MaxSafeRunnigTasks;

        /// <summary>
        /// Построен на использовании DI контейнера
        /// </summary>
        /// <param name="serviceProvider"></param>
        public TaskRunManager(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
            logger = serviceProvider.GetRequiredService<ILoggerFactory>().CreateLogger("Schedule runner");
        }

        /// <summary>
        /// Run new task (try)
        /// </summary>
        /// <param name="workset"></param>
        /// <returns></returns>
        public bool TryRun(Workset workset)
        {           
            //check if too much
            if (IsBusy)
            {
                logger.LogWarning("There's no free slots to run new tasks");
                return false;
            }

            //dont launch schedule if alredy runnin
            if (WorkSets.Any(t => t.WorkId == workset.WorkId))
            {
                logger.LogInformation($"Already running schedule with id {workset.WorkId}");
                return false;
            }


            //create "real" task
            var task = Task.Run(() =>
            {
                var worker = (AbstractWorker)serviceProvider.GetService(workset.WorkerType);

                workset.WorkState = WorkState.Work;
                worker.Work(workset.Argument, workset.CancellationTokenSource.Token);
            });

            workset.AssignTask(task);

            task.ContinueWith(t =>
            {
                if (!WorkSets.TryTake(out workset))
                {
                    throw new Exception($"Cannot take out finished work task {workset.WorkId}");
                }
            });

            if (!WorkSets.TryAdd(workset))
            {
                throw new Exception("Cannot add task to collection");
            }

            return true;
        }

        /// <summary>
        /// Проверить было ли расписание уже запущено
        /// </summary>
        /// <param name="workId"></param>
        /// <returns></returns>
        public bool IsInTasks(long workId)
        {
            return WorkSets.Any(t => t.WorkId == workId);
        }

        /// <summary>
        /// Cancel selected scheduled task
        /// </summary>
        /// <param name="workId"></param>
        public void Stop(long workId)
        {
            var task = WorkSets.FirstOrDefault(ts => ts.WorkId == workId);
            if (task == null)
            {
                throw new Exception($"Task not {workId} found");
            }

            if (task.WorkState == WorkState.Work)
            {
                task.WorkState = WorkState.Cancellation;
                task.CancellationTokenSource.Cancel();
                while (!task.Task.IsCompleted)
                {
                    Thread.Sleep(100);
                }
                task.WorkState = WorkState.Idle;
            }
        }

        /// <summary>
        /// Отменить все в отдельном task
        /// </summary>
        /// <returns></returns>
        public Task StopAll()
        {
            return Task.Run(() => 
            {
                foreach (var ws in WorkSets)
                {
                    Stop(ws.WorkId);
                }
            });
        }
    }


}
