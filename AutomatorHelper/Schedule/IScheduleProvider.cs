﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AutomatorHelper.Schedule
{   
    /// <summary>
    /// 
    /// </summary>
    public interface IScheduleProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<ISchedule>> GetSchedules();
    }
}
