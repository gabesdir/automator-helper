﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace AutomatorHelper.Schedule
{
    /// <summary>
    /// Хелпер конфигурации провайдеров и воркеров
    /// </summary>
    public class ScheduleConfiguration
    {
        private readonly List<Type> _workers = new List<Type>();
        public ReadOnlyCollection<Type> Workers => _workers.AsReadOnly();

        private List<Type> _providers = new List<Type>();
        public ReadOnlyCollection<Type> Providers => _providers.AsReadOnly();

        public void AddProvider<TProvider>()
            where TProvider: IScheduleProvider
        {
            _providers.Add(typeof(TProvider));
        }

        public void AddWorker<TWorker>()
            where TWorker: IWorker
        {
            _workers.Add(typeof(TWorker));
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавить таску с поллингом расписаний от указанных провайдеров расписаний, основана на RecurrentTasks
        /// </summary>
        public static IServiceCollection AddSchedules(this IServiceCollection services, Action<ScheduleConfiguration> configure)
        {
            //do configuration
            var configuration = new ScheduleConfiguration();
            configure(configuration);

            //register providers
            foreach (var type in configuration.Providers)
            {
                if (!services.Any(s => s.ServiceType == type))
                {
                    services.AddSingleton(type);
                }
            }

            //add picker
            services.AddSingleton(sp =>
            {
                var schedulePicker = new SchedulePicker();
                foreach (var providerType in configuration.Providers)
                {
                    //get instances of IScheduleProvider implementation
                    schedulePicker.AddScheduleProvider((IScheduleProvider)sp.GetRequiredService(providerType));
                }
                return schedulePicker;
            });

            //register workers
            foreach (var type in configuration.Workers)
            {
                if (!services.Any(s => s.ServiceType == type))
                {
                    services.AddTransient(type);
                }
            }

            //di run manager
            services.AddSingleton<TaskRunManager>();
            //add recurent task
            services.AddTask<SchedulePollerTask>(opts =>
            {
                opts.FirstRunDelay = TimeSpan.FromMinutes(1) - TimeSpan.FromSeconds(DateTime.Now.Second); //to minutes
                opts.Interval = TimeSpan.FromMinutes(1);
            });

            return services;
        }
    }
}
