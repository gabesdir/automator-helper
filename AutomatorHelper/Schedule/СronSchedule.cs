﻿using Cronos;
using System;
using System.Collections.Generic;

namespace AutomatorHelper.Schedule
{
    /// <summary>
    ///  Заглушка для интерфейса ISchedule
    /// </summary>
    public class CronSchedule : ISchedule
    {
        /// <summary>
        /// Id расписания, должен быть уникальным
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Cron выражение (до минут)
        /// </summary>
        public string CronString { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object Arg { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Type WorkerType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool WorkInParallel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public HashSet<long> Conflicts { get; set; } = new HashSet<long>();

        /// <summary>
        /// Самоопределение времени совпадения
        /// </summary>
        /// <returns></returns>
        public bool IsOccurentIn(DateTimeOffset date)
        {
            var parsed = CronExpression.Parse(CronString, CronFormat.Standard);
            var closest = parsed.GetNextOccurrence(date, TimeZoneInfo.Local, true);

            return closest.HasValue && closest == date;
        }
    }
}
