﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace AutomatorHelper.Schedule
{
    internal class ScheduleEqualityComparerId : IEqualityComparer<ISchedule>
    {
        public bool Equals(ISchedule x, ISchedule y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(ISchedule obj)
        {
            return obj.Id.GetHashCode();
        }
    }

    /// <summary>
    /// Проски для таблицы расписаний
    /// </summary>
    public class TimeTable: HashSet<ISchedule>
    {
        /// <summary>
        /// 
        /// </summary>
        public TimeTable() : base(new ScheduleEqualityComparerId()){}


        /// <summary>
        /// Создание таблицы из начальной коллекции
        /// </summary>
        /// <param name="schedules"></param>
        /// <returns></returns>
        public static TimeTable FromIEnumerable(IEnumerable<ISchedule> schedules)
        {
            var table = new TimeTable();
            foreach (var sc in schedules)
            {
                table.Add(sc);
            }
            return table;
        }
    }
}
