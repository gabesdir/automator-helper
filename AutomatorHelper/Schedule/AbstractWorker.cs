﻿using System.Threading;
using System.Threading.Tasks;

namespace AutomatorHelper.Schedule
{
    /// <summary>
    /// Worker, который уже содержит заглушку для синхронного запуска
    /// </summary>
    public abstract class AbstractWorker : IWorker
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="workset"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public abstract Task WorkAsync(object workset, CancellationToken cancellationToken);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workset"></param>
        /// <param name="cancellationToken"></param>
        public void Work(object workset, CancellationToken cancellationToken)
        {
            this.WorkAsync(workset, cancellationToken).GetAwaiter().GetResult(); //ha-ha
        }
    }
}
