﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RecurrentTasks;

namespace AutomatorHelper.Schedule
{
    /// <summary>
    /// Таска через Picker опрашивает провайдеров расписаний на совпадение.
    /// </summary>
    public class SchedulePollerTask : IRunnable
    {
        private readonly SchedulePicker schedulePicker;
        private readonly ILogger logger;
        private readonly TaskRunManager runManager;

        /// <summary>
        /// 
        /// </summary>
        public SchedulePollerTask(ILoggerFactory loggerFactory, SchedulePicker schedulePicker, TaskRunManager runManager)
        {
            this.schedulePicker = schedulePicker;
            this.runManager = runManager;
            logger = loggerFactory.CreateLogger("Task poller");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="scopeServiceProvider"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task RunAsync(ITask currentTask, IServiceProvider scopeServiceProvider, CancellationToken cancellationToken)
        {
            var occs = await schedulePicker.GetNowOccurences();

            if (occs.Count == 0)
            {
                logger.LogDebug($"No matches found for {DateTime.Now}, wait next launch");
                return;
            }
            else
            {
                logger.LogInformation($"Found {occs.Count} schedules to run");
            }

            //filter already running (to skip them all)
            var notRunning = occs
                .Where(o => !runManager.IsInTasks(o.Id))
                .OrderBy(o => o.Priority)
                .ToList();

            //Get highest schedule
            var highestSchedule = notRunning.First();

            //if highest is single
            if (!highestSchedule.WorkInParallel)
            {
                logger.LogDebug("Use single schedule by highest priority");
                //single logic check
                var priority = GetHighestWorkPriority();
                if (highestSchedule.Priority <= priority)
                {
                    //interrrupt
                    logger.LogDebug($"Got it: new single task priority({highestSchedule.Priority}) is better than current({priority})");
                    await runManager.StopAll(); //stop all anyway
                    runManager.TryRun(new Workset(highestSchedule));
                    return;
                }
                //or skip
                logger.LogWarning($"Fail: new single task priority({highestSchedule.Priority}) is worse than current({priority}) skippping...");
                return;
            }
            //check multiple conditions
            else
            {
                logger.LogDebug("Use multi-check logic");
                //filter all single schedules
                notRunning = notRunning.Where(s => s.WorkInParallel).ToList();
                highestSchedule = notRunning.First(); //reset highest parallel schedule

                //get most priority workset
                var highestWork = runManager.WorkSets.OrderBy(ws => ws.Priority).DefaultIfEmpty().FirstOrDefault();

                if (highestWork != null)
                {
                    if (!highestWork.CanParallel)
                    {
                        logger.LogDebug("Workset has single task - let's compare priority");
                        //compare priorities
                        if (highestSchedule.Priority < highestWork.Priority)
                        {
                            //interrupt
                            logger.LogDebug($"Got it: schedules task priority({highestSchedule.Priority}) is better than current({highestWork.Priority})");
                            await runManager.StopAll();
                            //run all
                            foreach (var s in FilterLocalConflicts(notRunning))
                            {
                                runManager.TryRun(new Workset(s));
                            }
                            return;
                        }
                        logger.LogWarning($"Fail: schedule priority({highestSchedule.Priority}) is worse than current single work({highestWork.Priority}) skippping...");
                        return;
                    }
                }
                //else add schedules to running tasks
                var filtered = FilterLocalConflicts(notRunning);
                RunMultipleWithConflictsResolve(filtered);
            }
        }

        //you shouldn't run without local conflicts filtering before
        private void RunMultipleWithConflictsResolve(IEnumerable<ISchedule> schedules)
        {
            foreach (var s in schedules)
            {
                //find conflicts in current work
                var conflicts = runManager.WorkSets.Where(ws => s.Conflicts.Contains(ws.WorkId));

                if (conflicts.Count() == 0)
                {
                    logger.LogDebug($"Schedule {s.Id} has no conflicts - just run");
                    runManager.TryRun(new Workset(s));
                    continue;
                }

                if (conflicts.Any(c => c.Priority < s.Priority))
                {
                    logger.LogDebug($"Schedule {s.Id} conflicts with current work but has lower priority - skip...");
                    continue;
                }
                else
                {
                    logger.LogDebug($"Schedule {s.Id} conflicts with current work but has BETTER priority - interrupt conflicts, run schedule");
                    foreach (var c in conflicts)
                    {
                        runManager.Stop(c.WorkId);
                    }
                    runManager.TryRun(new Workset(s));
                }
            }
        }

        private IEnumerable<ISchedule> FilterLocalConflicts(IEnumerable<ISchedule> schedules)
        {
            var result = new List<ISchedule>();
            foreach (var s in schedules)
            {
                bool bannedByConflict = schedules.Any(sc => s.Conflicts.Contains(sc.Id) && sc.Priority < s.Priority);
                if (!bannedByConflict)
                {
                    result.Add(s);
                    logger.LogDebug($"Schedule with id {s.Id} pass local conflicts check");
                }
                else
                {
                    logger.LogDebug($"Schedule with id {s.Id} BANNED by local conflicts check");
                }
            }
            return result;
        }

        private int GetHighestWorkPriority()
        {
            if (runManager.WorkSets.Count == 0)
            {
                logger.LogWarning($"Manager tasks is empty: return max(worse) {Int32.MaxValue} priority");
                return Int32.MaxValue;
            }
            return runManager.WorkSets.Select(ws => ws.Priority).Min();
        }
    }
}
