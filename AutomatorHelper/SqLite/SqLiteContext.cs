﻿using Microsoft.EntityFrameworkCore;
using AutomatorHelper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.SqLite
{
    public class SqLiteContext : DbContext
    {
        /// <summary>
        /// Общий справочник (простые строковые значения)
        /// </summary>
        public DbSet<GlossaryItem> Glossary { get; set; }

        /// <summary>
        /// Таблица сессий
        /// </summary>
        public DbSet<Session> Sessions { get; set; }

        /// <summary>
        /// Справочник прокси
        /// </summary>
        public DbSet<ProxySettings> Proxies { get; set; }

        /// <summary>
        /// Таблица с задачами
        /// </summary>
        public DbSet<AutomatorTask> Tasks { get; set; }

        /// <summary>
        /// Таблица-связка для задач и сессий
        /// </summary>
        public DbSet<TaskSession> TaskSessions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=automator.db");
            optionsBuilder.EnableSensitiveDataLogging(true);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        }
    }
}
