﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Extensions
{
    public static class DateExtensions
    {
        /// <summary>
        /// Обрезает дату до указанного разряда времени (меньшие становятся 0)
        /// </summary>
        /// <param name="date"></param>
        /// <param name="roundTicks">Разряд в тиках (exmpl: tm.Trim(TimeSpan.TicksPerMinute) - обрезать до минут )</param>
        /// <returns></returns>
        private static DateTime Trim(this DateTime date, long roundTicks)
        {
            return new DateTime(date.Ticks - date.Ticks % roundTicks, date.Kind);
        }

        /// <summary>
        /// Обрезать до минут
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime TrimToMinutes(this DateTime date)
        {
            return date.Trim(TimeSpan.TicksPerMinute);
        }
    }
}
