﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AutomatorHelper.Extensions
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Получить описание класса (если есть)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetClassDescription<T>(this T value)
            where T: class
        {
            var descriptions = value.GetType().GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (descriptions.Count() == 0)
            {
                return null;
            }
            return ((DescriptionAttribute)(descriptions).First()).Description;
        }
    }
}
