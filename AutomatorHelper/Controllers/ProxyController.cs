﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutomatorHelper.Models;
using AutomatorHelper.SqLite;

namespace AutomatorHelper.Controllers
{
    public class ProxyController : Controller
    {
        private readonly SqLiteContext sqLite;

        public ProxyController(SqLiteContext sqLite)
        {
            this.sqLite = sqLite;
        }


        public IActionResult Index()
        {
            ViewData["ProxyTypes"] = sqLite.Glossary
                .AsNoTracking()
                .Where(g => g.Type == Models.GlossaryItem.ItemType.Proxytypes)
                .Select(g => g.Value)
                .ToArray();

            return View(sqLite.Proxies.ToList());
        }

        public IActionResult List()
        {
            return Json( sqLite.Proxies.ToArray() );
        }

        [HttpPost]
        public async Task<IActionResult> Add(ProxySettings proxy)
        {
            try
            {
                await sqLite.Proxies.AddAsync(proxy);
                await sqLite.SaveChangesAsync();
                return Content("added");
            }
            catch(Exception e)
            {
                return UnprocessableEntity(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Update(ProxySettings proxy)
        {
            try
            {
                sqLite.Proxies.Update(proxy);
                await sqLite.SaveChangesAsync();
                return Content("updated");
            }
            catch (Exception e)
            {
                return UnprocessableEntity(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Delete(long id)
        {
            try
            {
                var model = await sqLite.Proxies.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);
                sqLite.Proxies.Remove(model);
                await sqLite.SaveChangesAsync();
                return Content("removed");
            }
            catch (Exception e)
            {
                return UnprocessableEntity(e.Message);
            }
        }
    }
}
