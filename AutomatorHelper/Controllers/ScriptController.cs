﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.NodeServices;
using Microsoft.Extensions.Configuration;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutomatorHelper.Views.ViewModels;
using AutomatorHelper.Models;

namespace AutomatorHelper.Controllers
{
    public class ScriptController : Controller
    {
        private readonly INodeServices nodeServices;

        private readonly string scriptsPath;
        private readonly string templatePath;
        private readonly string entryPointPath;

        public ScriptController(INodeServices nodeServices, IConfiguration configuration)
        {
            this.nodeServices = nodeServices;
            scriptsPath = configuration.GetValue<string>("Scripts") ?? "Scripts";
            templatePath = Path.Combine(scriptsPath, "tmpl/template.js");
            entryPointPath = Path.Combine(scriptsPath, "tmpl/system.js");
        }

        public IActionResult Index()
        {
            if (!Directory.Exists(scriptsPath))
            {
                return BadRequest("No scripts folder exists");
            }
            var allScripts = Directory.EnumerateFiles(scriptsPath).Select(s => Path.GetFileName(s));
            ViewData["Scripts"] = allScripts;

            return View("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Add()
        {
            return await GetEdit(null); 
        }

        [HttpGet("/script/edit")]
        public async Task<IActionResult> GetEdit(string name)
        {
            var model = new ScriptViewModel();
            if (name == null)
            {
                model.Name = $"Script_{DateTime.Now.Ticks}.js";
                model.Source = await System.IO.File.ReadAllTextAsync(templatePath);
                ViewData["Behavior"] = "add";
            }
            else
            {
                var script = FilterName(name);
                if (System.IO.File.Exists(script) == false)
                {
                    return BadRequest($"No file with name `{name}` exists");
                }

                model.Name = name;
                model.Source = await System.IO.File.ReadAllTextAsync(script);
                ViewData["Behavior"] = "edit";
            }
            return View("Edit", model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ScriptViewModel model)
        {
            if(model != null)
            {
                try
                {
                    var script = FilterName(model.Name);
                    await System.IO.File.WriteAllTextAsync(script, model.Source);
                    return await GetEdit(Path.GetFileName(script));
                }
                catch(Exception e)
                {
                    return BadRequest($"{e}");
                }
                
            }
            return BadRequest("No correct data provided");
        }

        [HttpGet]
        public IActionResult Delete(string name)
        {
            try
            {
                var script = FilterName(name);
                if (System.IO.File.Exists(script))
                {
                    System.IO.File.Delete(script);
                    return Index();
                }
                throw new FileNotFoundException(scriptsPath);
            }
            catch(Exception e)
            {
                return BadRequest($"Error: {e}");
            }
        }

        [HttpGet]
        public async Task<IActionResult> TestInvoke(string name)
        {
            return await Invoke(name, new AutomatorRequest { Html = "dup" });
        }

        [NonAction]
        private string FilterName(string name)
        {
            if(string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("script name");
            }
            if(name.EndsWith(".js") == false)
            {
                name += ".js";
            }
            return Path.Combine(scriptsPath, name).Replace(@"\", "/");          
        }

        [Route("/invoke/{name}")]
        public async Task<IActionResult> Invoke([FromRoute]string name, AutomatorRequest request)
        {
            try
            {
                var script = name.Replace(".js", string.Empty);
                request.Script = script;
                return Content(await nodeServices.InvokeAsync<string>(entryPointPath, request));
            }
            catch(Exception e)
            {
                return BadRequest($"{e}");
            }
        }
    }
}
