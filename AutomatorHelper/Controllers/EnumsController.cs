﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutomatorHelper.Models;
using AutomatorHelper.SqLite;
using AutomatorHelper.Views.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using static AutomatorHelper.Models.GlossaryItem;

namespace AutomatorHelper.Controllers
{
    public class EnumsController : Controller
    {
        private readonly SqLiteContext sqLite;

        public EnumsController(SqLiteContext sqLite)
        {
            this.sqLite = sqLite;
        }

        [NonAction]
        private ItemType GetEnumValue(string name)
        {
            return (ItemType)Enum.Parse(typeof(ItemType), name, true);
        }

        [NonAction]
        private string GetEnumDescription(ItemType en)
        {
            var info = en.GetType().GetMember(en.ToString()).FirstOrDefault();
            return info?.GetCustomAttribute<DescriptionAttribute>()?.Description ?? null;
        }

        [HttpGet("/enums/{entityName}/index")]
        public IActionResult ListEnums(string entityName)
        {
            var enumval = GetEnumValue(entityName);
            ViewData["Dataset"] = sqLite.Glossary
                .AsNoTracking()
                .Where(g => g.Type == enumval)
                .ToList();

            ViewData["EnumName"] = entityName;
            ViewData["EnumTitle"] = GetEnumDescription(enumval);

            return View("_EnumPage");
        }

        [HttpGet("/enums/{entityName}/delete")]
        public async Task<IActionResult> Delete([FromRoute]string entityName, [FromQuery]long id)
        {
            try
            {
                var item = await sqLite.Glossary
                    .AsNoTracking()
                    .FirstOrDefaultAsync(g => g.Id == id);

                sqLite.Glossary.Remove(item);
                await sqLite.SaveChangesAsync();               
                return Ok("deleted");
            }
            catch(Exception e) { return UnprocessableEntity(e.Message); }
        }

        [HttpPost("/enums/{entityName}/add")]
        public async Task<IActionResult> Add([FromRoute]string entityName, [FromForm]GlossaryItem model)
        {
            try
            {
                model.Type = GetEnumValue(entityName);
                await sqLite.Glossary.AddAsync(model);
                await sqLite.SaveChangesAsync();
                return Ok("added");
            }
            catch (Exception e) { return UnprocessableEntity(e.Message); }
        }

        [HttpPost("/enums/{entityName}/update")]
        public async Task<IActionResult> Update([FromRoute]string entityName, [FromForm]GlossaryItem model)
        {
            try
            {
                model.Type = GetEnumValue(entityName);
                sqLite.Glossary.Update(model);
                await sqLite.SaveChangesAsync();                
                return Ok("updated");
            }
            catch (Exception e) { return UnprocessableEntity(e.Message); }
        }
    }
}
