﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CronExpressionDescriptor;
using Cronos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutomatorHelper.Models;
using AutomatorHelper.SqLite;
using AutomatorHelper.Services;
using System.Text;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Http;

namespace AutomatorHelper.Controllers
{
    public class TaskController : Controller
    {
        private readonly SqLiteContext sqLite;
        private readonly TaskParser taskParser;

        public TaskController(SqLiteContext sqLite, TaskParser taskParser)
        {
            this.sqLite = sqLite;
            this.taskParser = taskParser;
        }


        public async Task<IActionResult> Index()
        {
            ViewData["Dataset"] = await sqLite.Tasks
                .AsNoTracking()
                .Include(t => t.TaskSessions)
                    .ThenInclude(ts => ts.SessionNavigation)
                .ToListAsync();

            return base.View();
        }

        [HttpGet]
        public async Task<IActionResult> ViewTask(long? taskId)
        {
            if (taskId.HasValue)
            {
                ViewData["Behavior"] = "edit";
                var aTask = await sqLite.Tasks.AsNoTracking()
                    .Include(t => t.TaskSessions)
                        .ThenInclude(ts => ts.SessionNavigation)
                    .FirstOrDefaultAsync(t => t.Id == taskId);

                return View("_PartialTask", aTask);
            }
            else
            {
                ViewData["Behavior"] = "add";
                return View("_PartialTask", null);
            }
        }

        [HttpPost]
        public async Task<IActionResult> EditTask(AutomatorTask model)
        {
            try
            {
                if (model.Id == 0)
                {
                    await sqLite.Tasks.AddAsync(model);
                }
                else
                {
                    //delete unlinked rows
                    var removedTaskSessions = (await sqLite.TaskSessions
                        .AsNoTracking()
                        .Where(ts => ts.TaskId == model.Id)
                        .ToListAsync())
                        .Except(model.TaskSessions);

                    sqLite.TaskSessions.RemoveRange(removedTaskSessions);

                    sqLite.Tasks.Update(model);
                }
                    
                await sqLite.SaveChangesAsync();
                return RedirectToAction("ViewTask", new { taskId = model.Id });
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Delete(long? taskId)
        {
            if (taskId.HasValue)
            {
                try
                {
                    var model = await sqLite.Tasks
                        .Include(t => t.TaskSessions)
                        .FirstOrDefaultAsync(t => t.Id == taskId);

                    if (model == null)
                    {
                        Response.StatusCode = 400;
                        return Content("Task id not found");
                    }

                    sqLite.Tasks.Remove(model);
                    await sqLite.SaveChangesAsync();
                    return Ok();
                }
                catch (Exception e)
                {
                    Response.StatusCode = 400;
                    return Content(e.Message);
                }
            }
            else
            {
                Response.StatusCode = 400;
                return Content("Task id does not specified");
            }
        }


        [HttpGet]
        public IActionResult Expression(string e)
        {
            CronExpression cronExpression;
            try
            {
                cronExpression = CronExpression.Parse(e);
                var descr = ExpressionDescriptor.GetDescription(e, new CronExpressionDescriptor.Options()
                {
                    DayOfWeekStartIndexZero = false,
                    Use24HourTimeFormat = true,
                    Locale = "ru"
                });
                return Content(descr);
            }
            catch(Exception exc)
            {
                Response.StatusCode = 400;
                return Content(exc.Message);
            }
        }

        [HttpGet]
        public IActionResult Import()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Import(IFormFile importTask)
        {
            try
            {
                var data = await taskParser.ParseFile(importTask.OpenReadStream());
                var converted = new List<AutomatorTask>();
                List<string> skippedSessions = new List<string>();
                List<string> addedSessions = new List<string>();

                foreach (var t in data)
                {
                    var tsk = new AutomatorTask()
                    {
                        ControlUrl = t.ControlUrl,
                        Cron = t.Cron
                    };

                    foreach(var s in t.Sessions)
                    {
                        var sessionByName = await sqLite.Sessions.AsNoTracking().FirstOrDefaultAsync(sn => sn.SessionName == s.Name);
                        if(sessionByName == null || s.Urls.Count == 0)
                        {
                            skippedSessions.Add(s.Name);
                            continue;
                        }
                        //convert result
                        s.Urls.ForEach(su =>
                        {
                            tsk.TaskSessions.Add(new TaskSession
                            {
                                SessionId = sessionByName.Id,
                                Url = su,
                                //SessionNavigation = sessionByName,
                                TaskNavigation = tsk
                            });
                            addedSessions.Add(s.Name);
                        });
                    }

                    converted.Add(tsk);
                }

                await sqLite.Tasks.AddRangeAsync(converted.Where(t => t.TaskSessions.Count > 0));
                await sqLite.SaveChangesAsync();

                return Json(new
                {
                    skippedSessions,
                    addedSessions
                });
            }
            catch(Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
        } 

        [HttpGet]
        public FileContentResult DownloadTemplate()
        {
            return new FileContentResult(Encoding.UTF8.GetBytes(taskParser.GenerateTemplate()), "application/json")
            {
                FileDownloadName = "template.txt"
            };
        }
    }
}
