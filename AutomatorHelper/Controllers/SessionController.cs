﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutomatorHelper.Models;
using AutomatorHelper.SqLite;
using static AutomatorHelper.Models.GlossaryItem;
using AutomatorHelper.Services;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace AutomatorHelper.Controllers
{
    public class SessionController : Controller
    {
        private readonly SqLiteContext sqLite;

        public SessionController(SqLiteContext sqLite)
        {
            this.sqLite = sqLite;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ViewData["Dataset"] = await sqLite.Sessions.AsNoTracking().ToListAsync();
            return View();
        }

        [HttpGet]
        public IActionResult Add()
        {
            ViewData["Behavior"] = "add";
            return View("_PartialSession");
        }

        [HttpPost]
        public async Task<IActionResult> Add(Session model)
        {
            try
            {
                await sqLite.Sessions.AddAsync(model);
                await sqLite.SaveChangesAsync();
                return Ok("done");
            }
            catch(Exception e)
            {
                Response.StatusCode = 400;
                return Json(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(long id)
        {
            var model = await sqLite.Sessions
                .AsNoTracking()
                .FirstOrDefaultAsync(s => s.Id == id);

            ViewData["Behavior"] = "edit";
            return View("_partialSession", model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Session model)
        {
            try
            {
                sqLite.Sessions.Update(model);
                await sqLite.SaveChangesAsync();
                return Ok("done");
            }
            catch(Exception e)
            {
                Response.StatusCode = 400;
                return Json(e.Message);
            }
        }
        

        [HttpGet]
        public async Task<IActionResult> Delete(long id)
        {
            try
            {
                var model = await sqLite.Sessions.AsNoTracking().FirstOrDefaultAsync(s => s.Id == id);
                sqLite.Sessions.Remove(model);
                await sqLite.SaveChangesAsync();
                return Ok();
            }
            catch(Exception e)
            {
                Response.StatusCode = 400;
                return Json(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Copy(long id)
        {
            try
            {
                var original = await sqLite.Sessions
                    .AsNoTracking()
                    .FirstOrDefaultAsync(s => s.Id == id);

                original.SessionName += $" copy {DateTime.Now}";
                original.Id = 0; //drop id

                await sqLite.Sessions.AddAsync(original);
                await sqLite.SaveChangesAsync();
                return Ok("copied");
            }
            catch(Exception e)
            {
                Response.StatusCode = 400;
                return Json(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            return Json(
                await sqLite.Sessions
                    .AsNoTracking()
                    .Select(s => new {
                        id = s.Id,
                        name = s.SessionName,
                        ip = s.ProxyIp + ":" + s.ProxyPort + " " + s.ProxyType,
                        timezone = s.TimezoneName,
                        userAgent = s.UserAgent,
                        browser = s.Browser,
                        platform = s.Platform
                    })
                    .ToListAsync()
            );
        }

        public async Task<IActionResult> GlossaryHelp(ItemType type)
        {
            return Json(await sqLite.Glossary
                .AsNoTracking()
                .Where(i => i.Type == type)
                .ToListAsync()
            );
        }

        [HttpGet]
        public IActionResult Import()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Import(IFormFile importSession)
        {
            if(importSession == null)
            {
                Response.StatusCode = 400;
                return BadRequest("Файл не указан");
            }
            var result = await new SessionParser().Parse(importSession.OpenReadStream());

            if(result.Sessions.Count == 0)
            {
                return Json(new { errors = result.Errors });
            }

            try
            {
                await sqLite.Sessions.AddRangeAsync(result.Sessions);
                await sqLite.SaveChangesAsync();
                return Json(new
                {
                    errors = result.Errors,
                    names = result.Sessions.Select(s => s.SessionName)
                });
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public IActionResult DownloadTemplate()
        {
            var parser = new SessionParser();
            return File(Encoding.UTF8.GetBytes(parser.GenerateTemplate()), "text/plain", "template.txt");
        }
    }
}
