﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using AutomatorHelper.Options;
using AutomatorHelper.SqLite;
using AutomatorHelper.Views.ViewModels;

namespace AutomatorHelper.Controllers
{
    public class HomeController : Controller
    {
        private readonly SqLiteContext sqLite;
        private readonly IOptionsSnapshot<AutomatorOptions> automatorOptions;

        public HomeController(SqLiteContext sqLite, IOptionsSnapshot<AutomatorOptions> automatorOptions)
        {
            this.sqLite = sqLite;
            this.automatorOptions = automatorOptions;
        }

        public async Task<IActionResult> Index()
        {
            var model = new MainStats();
            model.SitesCount = await sqLite.Glossary.LongCountAsync(g => g.Type == Models.GlossaryItem.ItemType.Urls);
            model.TasksCount = await sqLite.Tasks.AsNoTracking().LongCountAsync();
            model.SessionsCount = await sqLite.Sessions.AsNoTracking().LongCountAsync();

            ViewData["Automator"] = automatorOptions.Value;
            ViewData["Stats"] = model;
            return View();
        }

        [HttpGet]
        public PhysicalFileResult GetDbFile()
        {
            var path = Environment.CurrentDirectory.Replace(@"\", "/") + "/automator.db";
            return PhysicalFile(path, "application/octet-stream", "automator.db");
        }
    }
}
