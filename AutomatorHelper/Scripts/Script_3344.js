module.exports = function (model) {

    // Пример набора последовательных действий
    let response = [
        { "method": "scrollTo", "params": [0, 100] },
        { "method": "mouseTo", "params": [500, 200] },
        { "method": "mouseTo", "params": [100, 100] },
        { "method": "waitFor", "params": [6] },
        { "method": "clickToElement", "params": ["#my-link"] }
    ];

    // При первом запросе автоматор пришлет html страницы
    // Также доступны поля model.url - адрес целевой страницы, model.sess - имя используемой сессии
    if (model.html) {
        // скрипт должен вернуть набор действий
        return response;
    }
    else {
        // иначе если поле model.html не задано - произошла ошибка при загрузке страницы в автоматоре
        // здесь можно обработать ошибку
        return null;
    }
};