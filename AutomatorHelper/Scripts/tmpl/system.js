﻿module.exports = function (callback, model) {
    //nodejs entry point
    let result = require("../" + model.script)(model);
    callback(null, JSON.stringify( result || ["dup"] ));
};