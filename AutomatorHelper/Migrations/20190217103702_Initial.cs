﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AutomatorHelper.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Glossary",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Value = table.Column<string>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Glossary", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Proxies",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    LocalProxyIp = table.Column<string>(nullable: true),
                    ProxyIp = table.Column<string>(nullable: true),
                    ProxyPort = table.Column<string>(nullable: true),
                    ProxyType = table.Column<string>(nullable: true),
                    RealProxyIp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proxies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sessions",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Browser = table.Column<string>(nullable: true),
                    Platform = table.Column<string>(nullable: true),
                    UnmaskedVendor = table.Column<string>(nullable: true),
                    SessionName = table.Column<string>(nullable: true),
                    Longitude = table.Column<decimal>(nullable: false),
                    Latitude = table.Column<decimal>(nullable: false),
                    Language = table.Column<string>(nullable: true),
                    UseIncognito = table.Column<bool>(nullable: false),
                    EnableServiceWorker = table.Column<bool>(nullable: false),
                    DisablePopups = table.Column<bool>(nullable: false),
                    UseLocalStorage = table.Column<bool>(nullable: false),
                    TimezoneName = table.Column<string>(nullable: true),
                    IsFakeAudio = table.Column<bool>(nullable: false),
                    IsFakeCanvas = table.Column<bool>(nullable: false),
                    IsFakeFonts = table.Column<bool>(nullable: false),
                    IsFakePlugins = table.Column<bool>(nullable: false),
                    IsFakeRects = table.Column<bool>(nullable: false),
                    IsFakeWebrtc = table.Column<bool>(nullable: false),
                    LocalProxyIp = table.Column<string>(nullable: true),
                    ProxyIp = table.Column<string>(nullable: true),
                    ProxyPort = table.Column<string>(nullable: true),
                    ProxyType = table.Column<string>(nullable: true),
                    RealProxyIp = table.Column<string>(nullable: true),
                    UserAgent = table.Column<string>(nullable: true),
                    ScreenWidth = table.Column<string>(nullable: true),
                    ScreenHeight = table.Column<string>(nullable: true),
                    NavigatorVendor = table.Column<string>(nullable: true),
                    NavigatorLanguage = table.Column<string>(nullable: true),
                    NavigatorLanguages = table.Column<string>(nullable: true),
                    NavigatorProductSub = table.Column<string>(nullable: true),
                    NavigatorHardwareConcurrency = table.Column<string>(nullable: true),
                    NavigatorMaxTouchPoints = table.Column<string>(nullable: true),
                    NavigatorPlatform = table.Column<string>(nullable: true),
                    NavigatorDoNotTrack = table.Column<bool>(nullable: true),
                    NavigatorGamepads = table.Column<bool>(nullable: false),
                    NavigatorBattery = table.Column<bool>(nullable: false),
                    NavigatorWebdriver = table.Column<bool>(nullable: false),
                    NavigatorOnLine = table.Column<bool>(nullable: false),
                    NavigatorMediaDeviceMemory = table.Column<bool>(nullable: false),
                    ScreenAvailHeight = table.Column<string>(nullable: true),
                    ScreenAvailWidth = table.Column<string>(nullable: true),
                    ScreenAvailTop = table.Column<string>(nullable: true),
                    ScreenAvailLeft = table.Column<string>(nullable: true),
                    ScreenColorDepth = table.Column<string>(nullable: true),
                    ScreenPixelDepth = table.Column<string>(nullable: true),
                    WindowDevicePixelRatio = table.Column<string>(nullable: true),
                    WindowInnerHeight = table.Column<string>(nullable: true),
                    WindowInnerWidth = table.Column<string>(nullable: true),
                    WindowOuterHeight = table.Column<string>(nullable: true),
                    WindowOutherWidth = table.Column<string>(nullable: true),
                    WindowPageXOffset = table.Column<string>(nullable: true),
                    WindowPageYOffset = table.Column<string>(nullable: true),
                    WindowScreenLeft = table.Column<string>(nullable: true),
                    WindowScreenTop = table.Column<string>(nullable: true),
                    WindowScreenX = table.Column<string>(nullable: true),
                    WindowScreenY = table.Column<string>(nullable: true),
                    BodyClientWidth = table.Column<string>(nullable: true),
                    BodyClientHeight = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sessions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ControlUrl = table.Column<string>(nullable: true),
                    CloseAfterDone = table.Column<bool>(nullable: false),
                    Cron = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TaskSessions",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TaskId = table.Column<long>(nullable: false),
                    SessionId = table.Column<long>(nullable: false),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskSessions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaskSessions_Sessions_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Sessions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskSessions_Tasks_TaskId",
                        column: x => x.TaskId,
                        principalTable: "Tasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TaskSessions_SessionId",
                table: "TaskSessions",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskSessions_TaskId",
                table: "TaskSessions",
                column: "TaskId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Glossary");

            migrationBuilder.DropTable(
                name: "Proxies");

            migrationBuilder.DropTable(
                name: "TaskSessions");

            migrationBuilder.DropTable(
                name: "Sessions");

            migrationBuilder.DropTable(
                name: "Tasks");
        }
    }
}
