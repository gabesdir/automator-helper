﻿document.addEventListener("DOMContentLoaded", function () {

    for (let stopClick of document.querySelectorAll(".stop-click-event")) {
        stopClick.addEventListener("click", function (event) {
            event.stopPropagation();
        });
    }

    for (let ip4Input of document.querySelectorAll(".ip4-validate")) {
        ip4Input.onkeyup = function () {
            const ipReg = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
            let vText = this.parentElement.querySelector(".valid-state");
            if (this.value.match(ipReg) === null) {
                vText.classList.add("red-text");
                vText.classList.remove("green-text");
                vText.textContent = "invalid ip";
            }
            else {
                vText.classList.add("green-text");
                vText.classList.remove("red-text");
                vText.textContent = "ip ok";
            }
        };
    }

    for (let portInput of document.querySelectorAll(".port-validate")) {
        portInput.onkeyup = function () {
            const portReg = /^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/;
            let pText = this.parentElement.querySelector(".valid-state");
            if (this.value.match(portReg) === null) {
                pText.classList.add("red-text");
                pText.classList.remove("green-text");
                pText.textContent = "invalid port";
            }
            else {
                pText.classList.remove("red-text");
                pText.classList.add("green-text");
                pText.textContent = "port ok";
            }
        };
    }
});

function makeRequest(url, method, data) {
    return new Promise((onResolve, onReject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method || "GET", url, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    onResolve(xhr.responseText);
                }
                else {
                    onReject(xhr);
                }
            }
        };
        xhr.send(data || null);
    });
}

function postRequest(url, data) {
    return makeRequest(url, "POST", data);
}

function getRequest(url, data) {
    return makeRequest(url, "GET", data);
}

function reloadAfter(delay) {
    setTimeout(() => location.reload(), delay || 500);
}