﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Models
{
    public class AutomatorRequest
    {
        public string Html { get; set; }
        public string Url { get; set; }
        public string Sess { get; set; }
        public string Script { get; set; }
    }
}
