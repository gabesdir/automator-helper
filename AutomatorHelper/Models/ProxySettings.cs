﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Models
{
    public class ProxySettings
    {
        [Key]
        public long Id { get; set; }

        public string LocalProxyIp { get; set; }

        public string ProxyIp { get; set; }

        public string ProxyPort { get; set; }

        public string ProxyType { get; set; }

        public string RealProxyIp { get; set; }
    }
}
