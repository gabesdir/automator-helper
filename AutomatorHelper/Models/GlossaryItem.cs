﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Models
{
    public class GlossaryItem
    {
        [Key]
        public long Id { get; set; }
        public string Value { get; set; }
        public string Comment { get; set; }

        public enum ItemType
        {
            [Description("Unknown type")]
            Unknown = 0,

            [Description("Языки")]
            Languages = 1,

            [Description("Платформы (ос)")]
            Platforms = 2,

            [Description("Браузеры")]
            Browsers = 3,

            [Description("Временные зоны")]
            Timezones = 4,

            [Description("Вендоры")]
            Vendors = 5,

            [Description("Типы прокси")]
            Proxytypes = 6,

            [Description("Юзер-агенты")]
            UserAgents = 7,

            [Description("Url для заданий")]
            Urls = 8,

            [Description("navigator.platform")]
            NavigatorPlatforms = 9,

            [Description("Варианты пиксельных величин")]
            Resolutions = 10,

            [Description("Управляющие url")]
            ControlUrls = 11
        }

        [BindNever]
        public ItemType Type { get; set; }
    }
}
