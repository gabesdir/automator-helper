﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Models
{
    public class SessionUrl
    {
        public long SessionId { get; set; }
        public string Url { get; set; }
    }

    /// <summary>
    /// Описывает полную модель генерации задания для автоматора
    /// </summary>
    public class AutomatorTask
    {
        /// <summary>
        /// Pre-init linked collections
        /// </summary>
        public AutomatorTask()
        {
            this.TaskSessions = new HashSet<TaskSession>();
        }

        [Key]
        [JsonIgnore]
        public long Id { get; set; }

        ///// <summary>
        ///// Включен/нет
        ///// </summary>
        //public bool Enabled { get; set; }

        /// <summary>
        /// Управляющий URL
        /// </summary>
        [JsonProperty("control_url")]
        public string ControlUrl { get; set; }

        /// <summary>
        /// Закрывать браузер после выполнения задания(й)
        /// </summary>
        [JsonProperty("close_after_done")]
        public bool CloseAfterDone { get; set; } = true;

        /// <summary>
        /// Сессии -> url, которые должны быть запущены (to JSON write-only)
        /// </summary>
        [JsonProperty("sessions")]
        [NotMapped]
        [BindNever]
        public Session[] Sessions { get; set; }

        /// <summary>
        /// Cron,  "* * * * *@* * * * * @"
        /// </summary>
        [JsonIgnore]
        public string Cron { get; set; }


        /// <summary>
        /// Ссслыка на связки с сессиями
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<TaskSession> TaskSessions { get; set; }
    }
}
