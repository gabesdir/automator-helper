﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using AutomatorHelper.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Models
{
    /// <summary>
    /// Описывает сессию для автоматора
    /// </summary>
    [JsonConverter(typeof(JsonPathConverter))]
    public class Session
    {
        /// <summary>
        /// Pre-init linked collection
        /// </summary>
        public Session()
        {
            this.TaskSessions = new HashSet<TaskSession>();
        }

        [Key]
        public long Id { get; set; }

        //generator settings
        [PlainMapper("browser")]
        [JsonPropertyPath("generator/browser")]
        public string Browser { get; set; } = "Google"; 

        [PlainMapper("platform")]
        [JsonPropertyPath("generator/platform")]
        public string Platform { get; set; } = "Android";

        [PlainMapper("unmaskedVendor")]
        [JsonPropertyPath("settings/unmaskedVendor")]
        public string UnmaskedVendor { get; set; } = "Google Inc.";

        #region Configure
        // Основные настройки сессии
        [PlainMapper("sessName")]
        [JsonPropertyPath("configure/sessName")]
        public string SessionName { get; set; } = $"Session_{DateTimeOffset.UtcNow.ToUnixTimeSeconds()}";

        [PlainMapper("longitude")]
        [JsonPropertyPath("configure/longitude")]
        public decimal Longitude { get; set; } = 54.231M;

        [PlainMapper("latitude")]
        [JsonPropertyPath("configure/latitude")]
        public decimal Latitude { get; set; } = 43.223M;

        [PlainMapper("language")]
        [JsonPropertyPath("configure/language")]
        public string Language { get; set; } = "ru";

        [PlainMapper("incognito")]
        [JsonPropertyPath("configure/incognito")]
        public bool UseIncognito { get; set; }

        [PlainMapper("enableServiceWorker")]
        [JsonPropertyPath("configure/enableServiceWorker")]
        public bool EnableServiceWorker { get; set; }

        [PlainMapper("disablePopups")]
        [JsonPropertyPath("configure/disablePopups")]
        public bool DisablePopups { get; set; }

        [PlainMapper("localStorage")]
        [JsonPropertyPath("configure/localStorage")]
        public bool UseLocalStorage { get; set; }

        [PlainMapper("timezoneName")]
        [JsonPropertyPath("configure/timezoneName")]
        public string TimezoneName { get; set; } = "Europe/Moscow";
        #endregion


        #region Fingerprints
        // Настройки подлинности браузера
        [PlainMapper("isFakeAudio")]
        [JsonPropertyPath("fingerprints/isFakeAudio")]
        public bool IsFakeAudio { get; set; }

        [PlainMapper("isFakeCanvas")]
        [JsonPropertyPath("fingerprints/isFakeCanvas")]
        public bool IsFakeCanvas { get; set; }

        [PlainMapper("isFakeFonts")]
        [JsonPropertyPath("fingerprints/isFakeFonts")]
        public bool IsFakeFonts { get; set; }

        [PlainMapper("isFakePlugins")]
        [JsonPropertyPath("fingerprints/isFakePlugins")]
        public bool IsFakePlugins { get; set; }

        [PlainMapper("isFakeRects")]
        [JsonPropertyPath("fingerprints/isFakeRects")]
        public bool IsFakeRects { get; set; }

        [PlainMapper("isFakeWebrtc")]
        [JsonPropertyPath("fingerprints/isFakeWebrtc")]
        public bool IsFakeWebrtc { get; set; }
        #endregion


        #region Network
        // Сетевые настройки сессии
        [PlainMapper("localProxyIp")]
        [JsonPropertyPath("network/localProxyIp")]
        public string LocalProxyIp { get; set; } = "192.168.10.4";

        [PlainMapper("proxyIp")]
        [JsonPropertyPath("network/proxyIp")]
        public string ProxyIp { get; set; } = "46.0.203.99";

        [PlainMapper("proxyPort")]
        [JsonPropertyPath("network/proxyPort")]
        public string ProxyPort { get; set; } = "20002";

        [PlainMapper("proxyType")]
        [JsonPropertyPath("network/proxyType")]
        public string ProxyType { get; set; } = "Http";

        [PlainMapper("realProxyIp")]
        [JsonPropertyPath("network/realProxyIp")]
        public string RealProxyIp { get; set; } = "46.0.203.99";
        #endregion


        #region Customize
        // Дополнительные настройки сессии
        [PlainMapper("userAgent")]
        [JsonPropertyPath("customize/userAgent")]
        public string UserAgent { get; set; } = "Mozilla/5.0 (Linux; Android 6.0; MX6 Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.85 Mobile Safari/537.36";

        [PlainMapper("screenWidth")]
        [JsonPropertyPath("customize/screenWidth")]
        public string ScreenWidth { get; set; } = "360";

        [PlainMapper("screenHeight")]
        [JsonPropertyPath("customize/screenHeight")]
        public string ScreenHeight { get; set; } = "640";

        [PlainMapper("navigator.vendor")]
        [JsonPropertyPath("customize/navigator.vendor")]
        public string NavigatorVendor { get; set; } = "Google Inc.";

        [PlainMapper("navigator.language")]
        [JsonPropertyPath("customize/navigator.language")]
        public string NavigatorLanguage { get; set; } = "ru-RU";

        [PlainMapper("navigator.languages")]
        [JsonPropertyPath("customize/navigator.languages")]
        public string NavigatorLanguages { get; set; } = "ru-RU,ru,en-US,en";

        [PlainMapper("navigator.productSub")]
        [JsonPropertyPath("customize/navigator.productSub")]
        public string NavigatorProductSub { get; set; } = "20030107";

        [PlainMapper("navigator.hardwareConcurrency")]
        [JsonPropertyPath("customize/navigator.hardwareConcurrency")]
        public string NavigatorHardwareConcurrency { get; set; } = "10";

        [PlainMapper("navigator.maxTouchPoints")]
        [JsonPropertyPath("customize/navigator.maxTouchPoints")]
        public string NavigatorMaxTouchPoints { get; set; } = "5";

        [PlainMapper("navigator.platform")]
        [JsonPropertyPath("customize/navigator.platform")]
        public string NavigatorPlatform { get; set; } = "Linux armv8l";

        [PlainMapper("navigator.doNotTrack")]
        [JsonPropertyPath("customize/navigator.doNotTrack")]
        public bool? NavigatorDoNotTrack { get; set; }

        [PlainMapper("navigator.gamepads")]
        [JsonPropertyPath("customize/navigator.gamepads")]
        public bool NavigatorGamepads { get; set; }

        [PlainMapper("navigator.battery")]
        [JsonPropertyPath("customize/navigator.battery")]
        public bool NavigatorBattery { get; set; }

        [PlainMapper("navigator.webdriver")]
        [JsonPropertyPath("customize/navigator.webdriver")]
        public bool NavigatorWebdriver { get; set; }

        [PlainMapper("navigator.onLine")]
        [JsonPropertyPath("customize/navigator.onLine")]
        public bool NavigatorOnLine { get; set; }

        [PlainMapper("navigator.mediaDeviceMemory")]
        [JsonPropertyPath("customize/navigator.mediaDeviceMemory")]
        public bool NavigatorMediaDeviceMemory { get; set; }

        [PlainMapper("screen.availHeight")]
        [JsonPropertyPath("customize/screen.availHeight")]
        public string ScreenAvailHeight { get; set; } = "640";

        [PlainMapper("screen.availWidth")]
        [JsonPropertyPath("customize/screen.availWidth")]
        public string ScreenAvailWidth { get; set; } = "360";

        [PlainMapper("screen.availTop")]
        [JsonPropertyPath("customize/screen.availTop")]
        public string ScreenAvailTop { get; set; } = "0";

        [PlainMapper("screen.availLeft")]
        [JsonPropertyPath("customize/screen.availLeft")]
        public string ScreenAvailLeft { get; set; } = "0";

        [PlainMapper("screen.colorDepth")]
        [JsonPropertyPath("customize/screen.colorDepth")]
        public string ScreenColorDepth { get; set; } = "24";

        [PlainMapper("screen.pixelDepth")]
        [JsonPropertyPath("customize/screen.pixelDepth")]
        public string ScreenPixelDepth { get; set; } = "24";

        [PlainMapper("window.devicePixelRatio")]
        [JsonPropertyPath("customize/window.devicePixelRatio")]
        public string WindowDevicePixelRatio { get; set; } = "2";

        [PlainMapper("window.innerHeight")]
        [JsonPropertyPath("customize/window.innerHeight")]
        public string WindowInnerHeight { get; set; } = "562";

        [PlainMapper("window.innerWidth")]
        [JsonPropertyPath("customize/window.innerWidth")]
        public string WindowInnerWidth { get; set; } = "360";

        [PlainMapper("window.outerHeight")]
        [JsonPropertyPath("customize/window.outerHeight")]
        public string WindowOuterHeight { get; set; } = "0";

        [PlainMapper("window.outherWidth")]
        [JsonPropertyPath("customize/window.outherWidth")]
        public string WindowOutherWidth { get; set; } = "0";

        [PlainMapper("window.pageXOffset")]
        [JsonPropertyPath("customize/window.pageXOffset")]
        public string WindowPageXOffset { get; set; } = "0";

        [PlainMapper("window.pageYOffset")]
        [JsonPropertyPath("customize/window.pageYOffset")]
        public string WindowPageYOffset { get; set; } = "0";

        [PlainMapper("window.screenLeft")]
        [JsonPropertyPath("customize/window.screenLeft")]
        public string WindowScreenLeft { get; set; } = "0";

        [PlainMapper("window.screenTop")]
        [JsonPropertyPath("customize/window.screenTop")]
        public string WindowScreenTop { get; set; } = "0";

        [PlainMapper("window.screenX")]
        [JsonPropertyPath("customize/window.screenX")]
        public string WindowScreenX { get; set; } = "0";

        [PlainMapper("window.screenY")]
        [JsonPropertyPath("customize/window.screenY")]
        public string WindowScreenY { get; set; } = "0";

        [PlainMapper("body.clientWidth")]
        [JsonPropertyPath("customize/body.clientWidth")]
        public string BodyClientWidth { get; set; } = "360";

        [PlainMapper("body.clientHeight")]
        [JsonPropertyPath("customize/body.clientHeight")]
        public string BodyClientHeight { get; set; } = "618";
        #endregion

        /// <summary>
        /// Write-Only: нужно только для того чтобы записать url внутрь сессии для записи задания в json
        /// </summary>
        [NotMapped]
        [BindNever]
        [JsonPropertyPath("start_url")]
        public string StartUrl { get; set; }


        /// <summary>
        /// Ссылка на связку с задачами
        /// </summary>
        public virtual ICollection<TaskSession> TaskSessions { get; set; }
    }
}
