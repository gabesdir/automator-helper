﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Models
{
    public class TaskSession
    {
        [Key]
        public long Id { get; set; }


        public long TaskId { get; set; }

        public long SessionId { get; set; }


        public string Url { get; set; }

        [ForeignKey("TaskId")]
        public AutomatorTask TaskNavigation { get; set; }

        [ForeignKey("SessionId")]
        public Session SessionNavigation { get; set; }
    }
}
