﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Views.ViewModels
{
    public class ScriptViewModel
    {
        public string Name { get; set; }

        public string Source { get; set; }
    }
}
