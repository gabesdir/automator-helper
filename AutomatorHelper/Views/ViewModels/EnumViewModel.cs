﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Views.ViewModels
{
    public class EnumViewModel
    {
        public string EnumName{ get; set; }
        public string EnumHelpUrl { get; set; } 
    }
}
