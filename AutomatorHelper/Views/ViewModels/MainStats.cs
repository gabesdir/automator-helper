﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Views.ViewModels
{
    public class MainStats
    {
        public long SitesCount { get; set; }

        public long SessionsCount { get; set; }

        public long TasksCount { get; set; }
    }
}
