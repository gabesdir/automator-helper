﻿using AutomatorHelper.Models;
using AutomatorHelper.Options;
using AutomatorHelper.SqLite;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatorHelper.Services
{
    public class TaskBuffer
    {
        public string ControlUrl { get; set; }
        public string Cron { get; set; }

        public List<SessionBuffer> Sessions { get; set; } = new List<SessionBuffer>();
    }

    public class SessionBuffer
    {
        public string Name { get; set; }
        public List<string> Urls { get; set; } = new List<string>();
    }

    public class TaskParser
    {
        private readonly IOptionsSnapshot<FormatOptions> options;

        public TaskParser(IOptionsSnapshot<FormatOptions> options)
        {
            this.options = options;
        }

        public async Task<List<TaskBuffer>> ParseFile(Stream fileStream)
        {
            using(var reader = new StreamReader(fileStream))
            {
                List<TaskBuffer> rawTasks = new List<TaskBuffer>();
                bool sessionReached = false;

                TaskBuffer tmpTask = null;
                SessionBuffer tmpSession = null;
                string line;
                while((line = await reader.ReadLineAsync()) != null)
                {
                    if (line.StartsWith(options.Value.TaskDelimiter))
                    {
                        //store task
                        tmpTask = new TaskBuffer();
                        tmpTask.ControlUrl = await reader.ReadLineAsync();

                        if(tmpTask.ControlUrl == null)
                        {
                            break;
                        }

                        tmpTask.Cron = await reader.ReadLineAsync();
                        rawTasks.Add(tmpTask);

                        sessionReached = false;
                        continue;
                    }

                    //sess
                    if (line.StartsWith(options.Value.SessionDelimiter))
                    {
                        tmpSession = new SessionBuffer();
                        tmpSession.Name = await reader.ReadLineAsync();
                        tmpTask.Sessions.Add(tmpSession);
                        sessionReached = true;
                        continue;
                    }

                    //read urls
                    if (sessionReached)
                    {
                        tmpSession.Urls.Add(line);
                    }
                }

                Validate(rawTasks);
                return rawTasks;
            }
        }


        private void Validate(List<TaskBuffer> tasks)
        {
            int linesCounter = 0;
            foreach(var t in tasks)
            {
                linesCounter++;
                //main
                if (t.ControlUrl.StartsWith(options.Value.SessionDelimiter) || t.ControlUrl.StartsWith(options.Value.TaskDelimiter))
                {
                    throw new FormatException($"Error on line {linesCounter}, control_url expected, got: {t.ControlUrl}");
                }
                linesCounter++;
                if (t.Cron.StartsWith(options.Value.SessionDelimiter) || t.Cron.StartsWith(options.Value.TaskDelimiter))
                {
                    throw new FormatException($"Error on line {linesCounter}, control_url expected, got: {t.Cron}");
                }

                foreach(var s in t.Sessions)
                {
                    linesCounter++;
                    if(s.Name.StartsWith(options.Value.SessionDelimiter) || s.Name.StartsWith(options.Value.SessionDelimiter))
                    {
                        throw new FormatException($"Error on line {linesCounter}, control_url expected, got: {s.Name}");
                    }
                    foreach(var u in s.Urls)
                    {
                        linesCounter++;
                        if (u.StartsWith(options.Value.SessionDelimiter) || u.StartsWith(options.Value.SessionDelimiter))
                        {
                            throw new FormatException($"Error on line {linesCounter}, control_url expected, got: {u}");
                        }
                    }
                }
            }
        }


        public string GenerateTemplate()
        {
            var sb = new StringBuilder();
            sb.AppendLine(GetTaskDelimiter());
            sb.AppendLine("%control_url%");
            sb.AppendLine("%Cron_template%");
            sb.AppendLine(GetSessionDelimiter());
            sb.AppendLine("%session_name_1%");
            sb.AppendLine("%session_url_1%");
            sb.AppendLine("%session_url_n%");
            sb.AppendLine(GetSessionDelimiter());
            sb.AppendLine("%session_name_2%");
            sb.AppendLine("%session_url_1%");
            sb.AppendLine("%session_url_n%");
            sb.Append(GetTaskDelimiter());
            return sb.ToString();
        }


        private string GetTaskDelimiter()
        {
            var data = string.Concat(Enumerable.Repeat(options.Value.TaskDelimiter, 48));
            return data;
        }


        private string GetSessionDelimiter()
        {
            var data = string.Concat(Enumerable.Repeat(options.Value.SessionDelimiter, 48));
            return data;
        }
    }
}
