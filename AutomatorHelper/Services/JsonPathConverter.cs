﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Services
{
    /// <summary>
    /// Аттрибут является флагом для конвертера из плоского объекта в древовидный json
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class JsonPropertyPath : Attribute
    {
        public JsonPropertyPath(string root)
        {
            this.RootName = root;
        }
        public string RootName { get; set; }
    }


    /// <summary>
    /// Конвертер который учитывает только аттрибут JsonPropertyPath
    /// </summary>
    public class JsonPathConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var tp = value.GetType();
            var props = value
                .GetType()
                .GetProperties()
                .Where(p => p.GetCustomAttributes(typeof(JsonPropertyPath), false).Count() > 0);

            JObject BuildJObject(IEnumerable<string> path, object deepValue)
            {
                var dataObj = new JObject();

                var next = path.Skip(1).FirstOrDefault();
                var currName = path.First();

                if (next == null)
                {
                    dataObj.Add(new JProperty(currName, deepValue));
                    return dataObj;
                }

                dataObj.Add(currName, BuildJObject(path.Skip(1), deepValue));

                return dataObj;
            }

            JObject j = new JObject();
            foreach (var p in props)
            {
                var path = (p.GetCustomAttributes(typeof(JsonPropertyPath), false).FirstOrDefault() as JsonPropertyPath).RootName;
                j.Merge(BuildJObject(path.Split('/'), p.GetValue(value)));
            }

            j.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException("Unnecessary because CanRead is false. The type will skip the converter.");
        }

        public override bool CanRead => false;

        //bad flag definition
        public override bool CanConvert(Type objectType)
        {
            return objectType.GetProperties()
                .Where(p => p.GetCustomAttributes(typeof(JsonPropertyPath), false).Count() > 0)
                .Count() > 0;
        }
    }
}
