﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using AutomatorHelper.Models;
using AutomatorHelper.Options;
using AutomatorHelper.Schedule;
using AutomatorHelper.SqLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AutomatorHelper
{
    public class AutomatorWorker : AbstractWorker
    {
        private readonly IOptionsSnapshot<AutomatorOptions> options;
        private readonly ILogger logger;

        public AutomatorWorker(IOptionsSnapshot<AutomatorOptions> options, ILoggerFactory loggerFactory)
        {
            this.options = options;
            logger = loggerFactory.CreateLogger("Automator worker");
        }

        public override Task WorkAsync(object workset, CancellationToken cancellationToken)
        {
            var aTask = (AutomatorTask)workset;
            string automatorLogMsg = $"Automator taskId: { aTask.Id}, when: { aTask.Cron}, ctrlUrl: { aTask.ControlUrl}";

            //gen json
            var jsonFileName = Path.Combine(options.Value.TempDirectory, Guid.NewGuid().ToString() + ".json");
            var json = JsonConvert.SerializeObject(aTask);
            Directory.CreateDirectory(options.Value.TempDirectory);
            File.WriteAllText(jsonFileName, json);

            //kill process
            if(options.Value.KillBeforeRun && !string.IsNullOrEmpty(options.Value.ProcessName))
            {
                logger.LogInformation($"Try Kill already running processes that has name: {options.Value.ProcessName}");

                var processToKill = Process.GetProcesses()
                    .Where(p => p.ProcessName.ToLower().Contains(options.Value.ProcessName.ToLower()));

                if (processToKill.Count() == 0)
                {
                    logger.LogInformation("No processes to kill, continue work...");
                }
                else
                {
                    logger.LogInformation($"Got {processToKill.Count()} processes, killing...");
                    foreach(var pr in processToKill)
                    {
                        try
                        {
                            logger.LogInformation($"Kill process [{pr.Id}]{pr.ProcessName}");
                            pr.Kill();
                        }
                        catch (Exception e)
                        {
                            logger.LogError($"Can't kill process [{pr.Id}]{pr.ProcessName} - {e.Message}");
                        }
                    }
                }
            }

            //start new
            Process process = new Process
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = options.Value.Executable,
                    Arguments = "--config-generator=" + jsonFileName,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    UseShellExecute = false
                }
            };

            try
            {
                Stopwatch timeoutSw = new Stopwatch(); //measuring timeout
                TimeSpan timeout = TimeSpan.FromMilliseconds(options.Value.ProcessTimeout);
                timeoutSw.Start();
                process.Start();

                while(!process.HasExited)
                {
                    //Cancelled by timeout
                    if(timeoutSw.Elapsed >= timeout)
                    {
                        logger.LogWarning($"Timeouted process [{timeout}] at " + automatorLogMsg);
                        process.Kill();
                        return Task.CompletedTask;
                    }

                    //Cancelled by schedule
                    if (cancellationToken.IsCancellationRequested)
                    {
                        logger.LogWarning($"Schedule is cancelled, " + automatorLogMsg);
                        process.Kill();
                        return Task.CompletedTask;
                    }

                    Thread.Sleep(100);
                }
                logger.LogInformation("Done work, " + automatorLogMsg);
            }
            catch (Exception e)
            {
                logger.LogError($"Automator work error: {e.Message}\n{e.StackTrace}\n{automatorLogMsg}");
            }
            finally
            {
                if (process != null && !process.HasExited)
                {
                    process.Kill();
                }
                File.Delete(jsonFileName); //remove json
            }

            return Task.CompletedTask;
        }
    }
}
