﻿using Microsoft.EntityFrameworkCore;
using AutomatorHelper.Schedule;
using AutomatorHelper.SqLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Services
{
    public class AutomatorScheduleProvider : IScheduleProvider
    {
        private readonly SqLiteContext sqLite;
        public AutomatorScheduleProvider(SqLiteContext sqLite)
        {
            this.sqLite = sqLite;
        }

        public async Task<IEnumerable<ISchedule>> GetSchedules()
        {
            return (await sqLite.Tasks.AsNoTracking()
                .Include(t => t.TaskSessions)
                    .ThenInclude(ts => ts.SessionNavigation)
                .ToListAsync())
                .Select(t =>
                {
                    //inject sessions as array
                    t.Sessions = t.TaskSessions.Select(ts =>
                    {
                        var s = ts.SessionNavigation;
                        s.StartUrl = ts.Url;
                        return s;
                    })
                    .ToArray();

                    //convert to ISchedule
                    return new CronSchedule
                    {
                        Arg = t,
                        CronString = t.Cron,
                        Priority = 0,
                        Id = t.Id,
                        WorkerType = typeof(AutomatorWorker),
                        WorkInParallel = false
                    };
                });
        }
    }
}
