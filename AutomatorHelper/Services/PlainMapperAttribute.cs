﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AutomatorHelper.Services
{
    [AttributeUsage(AttributeTargets.Property)]
    public class PlainMapperAttribute : Attribute
    {
        public PlainMapperAttribute(string plainName, bool required = false)
        {
            MappingName = plainName;
            Required = required;
        }

        public bool Required { get; }
        public string MappingName { get; }

        public static Dictionary<string, PropertyInfo> GetMapping(object target)
        {
            var result = target.GetType()
                .GetProperties()
                .Where(p => p.GetCustomAttribute<PlainMapperAttribute>() != null)
                .ToDictionary(p => p.GetCustomAttribute<PlainMapperAttribute>().MappingName, p => p);

            return result;
        }
    }
}
