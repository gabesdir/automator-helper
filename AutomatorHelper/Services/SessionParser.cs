﻿using AutomatorHelper.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AutomatorHelper.Services
{
    public class SessionParseResult
    {
        public List<Session> Sessions { get; set; } = new List<Session>();

        public List<string> Errors { get; set; } = new List<string>();
    }

    public class SessionParser
    {
        private const char SessionsDivider = '-';
        private const char PairDivider = '=';

        public async Task<SessionParseResult> Parse(Stream fileStream)
        {
            using (var reader = new StreamReader(fileStream))
            {
                long parseCounter = 1;
                SessionParseResult parseResult = new SessionParseResult();
                List<string> rawStrings = new List<string>();

                void DoParse()
                {
                    try
                    {
                        parseResult.Sessions.Add(Deserialize(rawStrings, parseCounter));
                    }
                    catch (Exception e)
                    {
                        parseResult.Errors.Add($"{e.GetType().Name}: {e.Message}");//something bad
                    }
                }

                string line;
                while ((line = await reader.ReadLineAsync()) != null)
                {
                    if (line.StartsWith(SessionsDivider))
                    {
                        DoParse();
                        parseCounter += rawStrings.Count + 1;
                        rawStrings.Clear();
                    }
                    else
                    {
                        rawStrings.Add(line);
                    }
                }

                if(rawStrings.Count > 0)
                {
                    DoParse();
                }

                return parseResult;
            }
        }

        public string GenerateTemplate()
        {
            var session = new Session();
            string result = "";
            var props = typeof(Session).GetProperties().Where(p => p.GetCustomAttribute<PlainMapperAttribute>() != null);
            foreach(var p in props)
            {
                var attr = p.GetCustomAttribute<PlainMapperAttribute>();
                result += $"{attr.MappingName} {PairDivider} {p.GetValue(session)?.ToString() ?? "null"}\n";
            }
            result += string.Concat(Enumerable.Repeat(SessionsDivider, 48));

            return result;
        }

        private Session Deserialize(List<string> lines, long fromCounter)
        {
            var from = fromCounter;
            var dummy = new Session();
            var mapping = PlainMapperAttribute.GetMapping(dummy);
            foreach(var line in lines)
            {
                var pair = line
                    .Split(PairDivider, StringSplitOptions.RemoveEmptyEntries)
                    .DefaultIfEmpty(string.Empty)
                    .Select(i => i.Trim())
                    .ToArray();

                if(pair.Length != 2)
                {
                    throw new Exception($"Invalid pair in: '{line}', missing key or value, must be %key%=%value% at line: {from}");
                }

                if (string.IsNullOrWhiteSpace(pair[0]))
                {
                    throw new Exception($"Invalid key in: '{line}' at line: {from}");
                }

                if (string.IsNullOrWhiteSpace(pair[1]))
                {
                    throw new Exception($"Invalid value in: '{line}' at line: {from}");
                }

                if (mapping.ContainsKey(pair[0]) == false)
                {
                    throw new Exception($"Unexpected key in: '{line}', no {nameof(PlainMapperAttribute)} provided, at line: {from}");
                }

                var targetProperty = mapping[pair[0]];
                try
                {
                    var convertedObject = SwitchConverter(pair[1], targetProperty.PropertyType);
                    targetProperty.SetValue(dummy, convertedObject);
                }
                catch(Exception e)
                {
                    throw new FormatException(e.Message + $" in: {line}, at line: {from}");
                }

                from++;
            }
            return dummy;
        }

        private object SwitchConverter(string value, Type type)
        {
            if (type == typeof(bool?))
            {
                var b = typeof(Nullable<>).GetGenericTypeDefinition();
                var a = type.GetGenericTypeDefinition();
                bool equals = b == a;
            }

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>).GetGenericTypeDefinition())
            {
                if(string.IsNullOrEmpty(value) || value.ToLower() == "null")
                {
                    return null;
                }
            }

            return Convert.ChangeType(value, type);
        }
    }
}
