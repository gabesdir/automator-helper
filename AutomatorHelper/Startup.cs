﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using AutomatorHelper.Models;
using AutomatorHelper.Options;
using AutomatorHelper.Schedule;
using AutomatorHelper.Services;
using AutomatorHelper.SqLite;

namespace AutomatorHelper
{
    public class Startup
    {
        private readonly IConfiguration configuration;
        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
       
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AutomatorOptions>(configuration.GetSection("AutomatorOptions"));
            services.Configure<FormatOptions>(configuration.GetSection("FormatOptions"));

            services.AddNodeServices();

            services.AddScoped<SqLiteContext>();
            services.AddTransient<TaskParser>();

            services.AddSchedules(conf => 
            {
                conf.AddProvider<AutomatorScheduleProvider>();
                conf.AddWorker<AutomatorWorker>();
            });

            services.AddMvc(opts => 
            {
                opts.ModelBinderProviders.Insert(0, new InvariantDecimalModelBinderProvider());
            });
        }

        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.ApplicationServices.GetRequiredService<SqLite.SqLiteContext>().Database.EnsureCreated();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
