﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Options
{
    public class AutomatorOptions
    {
        public string Executable { get; set; }
        public int ProcessTimeout { get; set; }
        public string TempDirectory { get; set; }

        public string ProcessName { get; set; }
        public bool KillBeforeRun { get; set; } = true;
    }
}
