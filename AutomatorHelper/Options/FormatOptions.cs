﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomatorHelper.Options
{
    public class FormatOptions
    {
        public string SessionDelimiter { get; set; }
        public string TaskDelimiter { get; set; }
    }
}
